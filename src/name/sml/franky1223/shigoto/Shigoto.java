package name.sml.franky1223.shigoto;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
 
public final class Shigoto extends JavaPlugin {
	
	HashMap<String, String> jobs = new HashMap<String, String>();
	
	@Override
    public void onEnable() {
		Set<String> players = getConfig().getKeys(false);
		int num = 0;
		while (players.iterator().hasNext()) {
			String player = players.iterator().next();
			jobs.put(player, getConfig().getString(player));
			num++;
		}
		getLogger().info(num+" players have jobs.");
		getLogger().info("Shigoto enabled!");
    }
	
    @Override
    public void onDisable() {
    	Set<Entry<String, String>> players = jobs.entrySet();
		int num = 0;
    	while (players.iterator().hasNext()){
    		Entry<String, String> player = players.iterator().next();
    		getConfig().set(player.getKey(), player.getValue());
			num++;
    	}
    	saveDefaultConfig();
    	saveConfig();
		getLogger().info(num+" jobs saved.");
		getLogger().info("Shigoto disabled!");
    }

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
	    	if (cmd.getName().equalsIgnoreCase("job")) {
	    		if (args.length > 0 && !args[0].isEmpty() && args[0].length() > 0) {
	    			String title = args[0];
	    			for (int i = 1; i < args.length; i++) {
	    				title = title.concat(" "+args[i]);
	    			}
	    			jobs.put(sender.getName(), title);
	    			((Player) sender).setDisplayName("["+title+"] "+sender.getName());
	    			sender.sendMessage("Your job has been set to "+title+".");
	    			return true;
	    		} else {
	    			jobs.remove(sender.getName());
	    			((Player) sender).setDisplayName(sender.getName());
	    			sender.sendMessage("Your job has been reset.");
	        		return true;
	    		}
	    	}
		}
		return false;
	}
	
}